package edu.duke.yz723.battleship;

import java.util.Random;

import java.lang.Math;


public class RandomGenerator {

    public Coordinate getRandomCoordination(int rowMax, int columnMax){
       return new Coordinate(randGenerator(0,rowMax), randGenerator(0,columnMax));
    }

    public Placement getRandomPlacement(int rowMax, int columnMax){
        Coordinate c = getRandomCoordination(rowMax, columnMax);
        char orien;
        int flag = randGenerator(0,6);
        if ( flag == 0){orien = 'U';}
        else if ( flag == 1){orien = 'R';}
        else if ( flag == 2){orien = 'D';}
        else if ( flag == 3){orien = 'L';}
        else if ( flag == 4){orien = 'V';}
        else{orien = 'H';}
        return new Placement(c,orien);
    }

    public int randGenerator(int min, int max){
        double rand = Math.random();
        return min + (int)((rand)*(max - min + 1));
    }

    public char getRandChar(){
        Random rand = new Random();

        if (randGenerator(0,3) == 0){
            return 'F';
        }
        if (randGenerator(0,3) == 1){
            return 'S';
        }
        if (randGenerator(0,3) == 2) {
            return 'M';
        }
        return 'F';
    }


}
